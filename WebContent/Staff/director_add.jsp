<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.io.*, java.util.*,cs425.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		int pid = Integer.parseInt(request.getParameter("pid"));
		String name = request.getParameter("name");
		String dob = request.getParameter("dob");
		String gender = request.getParameter("gender");
		String role = request.getParameter("role");
		CinemaTheatre cinema = new CinemaTheatre();

		Person person = new Person(pid, name, dob, gender);

		Director director = new Director(pid, name, dob, gender, role);
		cinema.insertPerson(person);
		cinema.insertDirector(director);
	%><jsp:forward page="movie_info.jsp" />
</body>
</html>