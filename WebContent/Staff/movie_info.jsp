<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.io.*, java.util.*,cs425.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form method="post" action="movie_add.jsp">
		ADD MOVIE <br>
		<input value="1" name="id_a" type="number" /> <br>
		<input value="Title" name="title_a" type="text" /> <br>
		<input value="Rating" name="rating_a" type="text" /> <br>
		<input value="Year" name="year_a" type="text" /> <br>
		<input value="Length" name="length_a" type="text" /> <br>
		<input value="Genre" name="genre_a" type="text" /> <br>
		<input value="Writer" name="writer_a" type="text" /> <br>
		<input value="Actor" name="actor_a" type="text" /> <br>
		<input value="Director" name="director_a" type="text" /> <br>
		<input type="submit" value="add">
	</form>
	<form method="post" action="movie_up.jsp">
		Update MOVIE <br>
		<input value="1" name="id_u" type="number" /> <br>
		<input value="Title" name="title_u" type="text" /> <br>
		<input value="Rating" name="rating_u" type="text" /> <br>
		<input value="Year" name="year_u" type="text" /> <br>
		<input value="Length" name="length_u" type="text" /> <br>
		<input value="Genre" name="genre_u" type="text" /> <br>
		<input value="Writer" name="writer_u" type="text" /> <br>
		<input value="Actor" name="actor_u" type="text" /> <br>
		<input value="Director" name="director_u" type="text" /> <br>
		<input type="submit" value="update">
	</form>
	<form action="movie_mod.jsp" method="post">
		DELETE MOVIE <br>
		<input value="1" name="id" type="number" /> <br>
		<input type="submit" value="delete">
	</form>
	<hr>
	<form action="add_actor.jsp" method="post">
		Add Actor <br>
		<input value="pid" name="pid" type="number" />
		<input value="name" name="name" type="text" />
		<input value="dob" name="dob" type="text" />
		<input value="gender" name="gender" type="text" />
		<input value="role" name="role" type="text" /> <br>
		<input type="submit" value="Add">
	</form>
	
	<form action="update_actor.jsp" method="post">
		UPDATE Actor <br>
		<input value="pid" name="pid" type="number" />
		<input value="name" name="name" type="text" />
		<input value="dob" name="dob" type="text" />
		<input value="gender" name="gender" type="text" />
		<input value="role" name="role" type="text" /> <br>
		<input type="submit" value="Add">
	</form>
	
	<form action="delete_actor.jsp" method="post">
		DELETE Actor <br>
		<input value="pid" name="pid" type="number" />
		<input type="submit" value="Add">
	</form>
	<hr>
	<form action="add_director.jsp" method="post">
		Add Director <br>
		<input value="pid" name="pid" type="number" />
		<input value="name" name="name" type="text" />
		<input value="dob" name="dob" type="text" />
		<input value="gender" name="gender" type="text" />
		<input value="role" name="role" type="text" /> <br>
		<input type="submit" value="Add">
	</form>
	
	<form action="update_director.jsp" method="post">
		UPDATE Director <br>
		<input value="pid" name="pid" type="number" />
		<input value="name" name="name" type="text" />
		<input value="dob" name="dob" type="text" />
		<input value="gender" name="gender" type="text" />
		<input value="role" name="role" type="text" /> <br>
		<input type="submit" value="Update">
	</form>
	
	<form action="delete_director.jsp" method="post">
		DELETE director <br>
		<input value="pid" name="pid" type="number" />
		<input type="submit" value="Delete">
	</form>
	<hr>
	<form action="add_writer.jsp" method="post">
		Add Writer <br>
		<input value="pid" name="pid" type="number" />
		<input value="name" name="name" type="text" />
		<input value="dob" name="dob" type="text" />
		<input value="gender" name="gender" type="text" />
		<input value="role" name="role" type="text" /> <br>
		<input type="submit" value="Add">
	</form>
	
	<form action="update_writer.jsp" method="post">
		UPDATE Writer <br>
		<input value="pid" name="pid" type="number" />
		<input value="name" name="name" type="text" />
		<input value="dob" name="dob" type="text" />
		<input value="gender" name="gender" type="text" />
		<input value="role" name="role" type="text" /> <br>
		<input type="submit" value="Update">
	</form>
	
	<form action="delete_writer.jsp" method="post">
		DELETE Writer <br>
		<input value="pid" name="pid" type="number" />
		<input type="submit" value="Delete">
	</form>
</body>
</html>