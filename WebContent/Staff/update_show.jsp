<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  import="java.io.*, java.util.*,cs425.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		int id = Integer.parseInt(request.getParameter("mid").trim());
		String title = request.getParameter("title");
		String start = request.getParameter("start");
		String end = request.getParameter("end");
		CinemaTheatre cinema = new CinemaTheatre();
		cinema.updateShowtime(id, title, start, end);
	%><jsp:forward page="showtimes.jsp" />
</body>
</html>