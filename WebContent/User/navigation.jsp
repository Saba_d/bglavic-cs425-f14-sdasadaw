<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<hr>
	<br>
	<a href="tickets.jsp" target="content">Tickets</a>
	<br>
	<hr>
	<br>
	<a href="rate.jsp" target="content">Rate Movies</a>
	<br>
	<hr>
	<br>
	<a href="available.jsp" target="content">Check Availability</a>
	<hr>
	<br>
	<form action="../search.jsp" method="POST" target="content">
		<input type="text" id="search" name="search" value="" /> <input
			type="checkbox" name="title" /> title <input type="checkbox"
			name="actor" /> actor <input type="checkbox" name="director" />
		director <input type="checkbox" name="writer" checked="checked" />
		writer<input type="submit" value=""
			style="background: url(../images/search.png) no-repeat" />
	</form>
	<hr>
	<br>
	<a href="../index.jsp" target="_top">Login Page</a>
	<hr>
	<script type="text/javascript"
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
	<script src="../js/jquery.autocomplete.js"></script>
	<script>
		$("#search").autocomplete("../getdata.jsp");
	</script>
</body>
</html>