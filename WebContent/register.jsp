<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.io.*, java.util.*, cs425.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		CinemaTheatre cinema = new CinemaTheatre();
		Random rand = new Random();
		int n = rand.nextInt(90000) + 10000;
	%>
	<center>
		<br> <br>Hi there! Please register here :)!<br> <br>
		<table>
			<tr>
				<td>
					<form method="post" action="register.jsp">
						<input type="text" name="new_name_u" value="name_u"> <br>
						<input type="text" name="new_user_u" value="username_u"> <br>
						<input type="text" name="new_password_u" value="Password_u">
						<br> <input type="text" name="c_password_u" value="confirm_u">
						<br> <br> <input type="submit" value="Sign Up as User!">
					</form>
				</td>
				<td>
					<form method="post" action="register.jsp">
						<input type="text" name="new_name_s" value="name_s"> <br>
						<input type="text" name="new_user_s" value="username_s"> <br>
						<input type="text" name="new_password_s" value="Password_s">
						<br> <input type="text" name="c_password_s" value="confirm_s">
						<br> <br> <input type="submit" value="Sign Up as staff!">
					</form>
				</td>
			</tr>
		</table>
	</center>
	<%
		try {
			String name_u = request.getParameter("new_name_u");
			String username_u = request.getParameter("new_user_u");
			String password_u = request.getParameter("new_password_u");
			String cpassword_u = request.getParameter("c_password_u");
			if (password_u.equals(cpassword_u)) {
				Users us = new Users(n, name_u, username_u, password_u);
				cinema.insertUser(us);
				Member member = new Member(n, name_u, username_u,
						password_u);
				cinema.insertMember(member);
	%><jsp:forward page="User/index_user.jsp" />
	<%
		} else {
				out.println("Passwords do no match");
			}
		} catch (Exception e) {
			out.println("");
		}
	%>
	<%
		try {
			String name_s = request.getParameter("new_name_s");
			String username_s = request.getParameter("new_user_s");
			String password_s = request.getParameter("new_password_s");
			String cpassword_s = request.getParameter("c_password_s");
			if (password_s.equals(cpassword_s)) {
				Users us = new Users(n, name_s, username_s, password_s);
				cinema.insertUser(us);
				Staff staff = new Staff(n, name_s, username_s, password_s);
				cinema.insertStaff(staff);
	%><jsp:forward page="Staff/index_staff.jsp" />
	<%
		} else {
				out.println("Passwords do no match");
			}
		} catch (Exception e) {
			out.println("");
		}
	%>
</body>
</html>