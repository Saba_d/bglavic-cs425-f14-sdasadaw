<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>DSsquare!</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script src="js/jquery.autocomplete.js"></script>
</head>
<body>

	<br><%!String[] search;%>

	<center>
		You have selected:
		<%
		search = request.getParameterValues("search");
		if (search != null) {
			for (int i = 0; i < search.length; i++) {

				if (("batman begins").contains(search[i].toLowerCase())) {
					out.println("<img src=images/bb.jpg style=float: left; position: relative;"
							+ "	height=175dp width=150dp />"
							+ "	<b><u>Batman Begins</u></b>"
							+ "	<br>"
							+ "	<br>After training with his mentor, Batman begins his war on crime"
							+ "	to free the crime-ridden Gotham City from corruption that the Scarecrow"
							+ "	and the League of Shadows have cast upon it."
							+ "	<br>"
							+ "	<br>Director: Christopher Nolan"
							+ "	<br>"
							+ "	<br>Writers: Bob Kane (characters), David S. Goyer"
							+ "	(story),Christopher Nolan (screenplay), David S. Goyer (screenplay)"
							+ "	<br>"
							+ "	<br>Stars: Christian Bale, Michael Caine, Ken Watanabe"
							+ "	<br>");

				} else if (("the dark knight").contains(search[i]
						.toLowerCase())) {
					out.println("<br><img src=images/tdk.jpg style=float: left; position: relative;"
							+ "height=175dp width=150dp />"
							+ "<b>"
							+ "<u>The Dark Knight</u></b>"
							+ "<br>"
							+ "<br>When the menace known as the Joker wreaks havoc and chaos on"
							+ "the people of Gotham, the caped crusader must come to terms with one of"
							+ "the greatest psychological tests of his ability to fight injustice."
							+ "<br>"
							+ "	<br>Director: Christopher Nolan"
							+ "<br>"
							+ "	<br>Writers: Jonathan Nolan (screenplay), Bob Kane (characters),"
							+ "	David S. Goyer (story),Christopher Nolan (screenplay), David S. Goyer"
							+ "	(screenplay)"
							+ "	<br>"
							+ "	<br>Stars: Christian Bale, Heath Ledger, Aaron Eckhart"
							+ "	<br>"
							+ "	<br>"
							+ "	<img src=images/tdkr.jpg style=float: left; position: relative;"
							+ "		height=175dp width=150dp />"
							+ "	<b><u>The Dark Knight Rises</u></b>"
							+ "<br>"
							+ "	<br>When Bane, a former member of the League of Shadows, plans to"
							+ "	continue the work of Ra's al Ghul, the Dark Knight is forced to return"
							+ "	after an eight year absence to stop him."
							+ "	<br>"
							+ "	<br>Director: Christopher Nolan"
							+ "	<br>"
							+ "<br>Writers: Jonathan Nolan (screenplay), Bob Kane (characters),"
							+ "	David S. Goyer (story),Christopher Nolan (screenplay), David S. Goyer"
							+ "	(screenplay)"
							+ "	<br>"
							+ "	<br>Stars: Christian Bale, Tom Hardy, Marion Cotillard"
							+ "	<br>");
				} else if (("the dark knight rises").contains(search[i]
						.toLowerCase())) {
					out.println("	<img src=images/tdkr.jpg style=float: left; position: relative;"
							+ "		height=175dp width=150dp />"
							+ "	<b><u>The Dark Knight Rises</u></b>"
							+ "<br>"
							+ "	<br>When Bane, a former member of the League of Shadows, plans to"
							+ "	continue the work of Ra's al Ghul, the Dark Knight is forced to return"
							+ "	after an eight year absence to stop him."
							+ "	<br>"
							+ "	<br>Director: Christopher Nolan"
							+ "	<br>"
							+ "<br>Writers: Jonathan Nolan (screenplay), Bob Kane (characters),"
							+ "	David S. Goyer (story),Christopher Nolan (screenplay), David S. Goyer"
							+ "	(screenplay)"
							+ "	<br>"
							+ "	<br>Stars: Christian Bale, Tom Hardy, Marion Cotillard"
							+ "	<br>");
				}
			}
		} else
			out.println("<u>none<u>");
	%>
	</center>
	<script>
		$("#search").autocomplete("/Final/source/getdata.jsp");
	</script>
</body>
</html>