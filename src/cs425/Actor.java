package cs425;

public class Actor extends Person{
	
	public Actor(int pid, String name, String dob, String gender, String role) {
		super(pid, name, dob, gender);
		this.role = role;
		this.pid = pid;
	}

	private String role;
	private int pid;
	

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}