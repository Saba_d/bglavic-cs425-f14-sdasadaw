package cs425;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CinemaTheatre {
	static Connection connection = null;
	static Statement statement = null;// for sql query
	static CinemaTheatre cinema = new CinemaTheatre();

	public Connection getConnection() throws SQLException {
		// String
		// url="jdbc:mysql://fourier.cs.iit.edu:1521/ORCL.CS.IIT.EDU?";//users=ssher&pswrd=sufyan1234";
		String url = "jdbc:oracle:thin:@fourier.cs.iit.edu:1521:orcl";
		try {
			// Connecting to Driver
			System.out.println("Connecting to Database...");
			Class.forName("oracle.jdbc.driver.OracleDriver");
			connection = DriverManager
					.getConnection(url, "ssher", "sufyan1234");
			// 1ST IS THE HOSTnm, PORT AND DATABSE nm
			// 2nd is usersnm 3rd is pswrd to the data base
			// Starting transaction statement
			connection.setAutoCommit(false);
			System.out.println("Connection to the Database Successful");
		} catch (Exception e) {
			System.out.println("Error : " + e.getMessage());
		}
		return connection;
	}

	public void closeConnection(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void createTable() throws SQLException {
		connection = cinema.getConnection();
		statement = connection.createStatement();
		try {
			statement
					.addBatch("create table users(usrid int NOT NULL, nm varchar(50), login varchar(20) NOT NULL, pswrd varchar(20), PRIMARY KEY(usrid))");
			statement
					.addBatch("create table member(usrid int,  PRIMARY KEY(usrid),  FOREIGN KEY (usrid) REFERENCES users(usrid) ON DELETE CASCADE)");
			statement
					.addBatch("create table staff(usrid int,  PRIMARY KEY(usrid),  FOREIGN KEY (usrid) REFERENCES users(usrid) ON DELETE CASCADE)");
			statement
					.addBatch("create table person(pid int NOT NULL, pnm varchar(25), dob varchar(20), gender char(1),  PRIMARY KEY (pid))");
			statement
					.addBatch("create table writer(pid int,  role varchar(20), PRIMARY KEY (pid),  FOREIGN KEY (pid) REFERENCES person(pid)ON DELETE CASCADE)");
			statement
					.addBatch("create table actor(pid int, role varchar(20),  PRIMARY KEY (pid),  FOREIGN KEY (pid) REFERENCES person(pid) ON DELETE CASCADE)");
			statement
					.addBatch("create table director(pid int NOT NULL, role varchar(20),  PRIMARY KEY (pid),  FOREIGN KEY (pid) REFERENCES person(pid) ON DELETE CASCADE)");
			statement
					.addBatch("create table movies(mid int NOT NULL, mtitle varchar(50) NOT NULL, pgrating varchar(10), release_year int, length varchar(30), genre varchar(10),  PRIMARY KEY (mid, mtitle))");
			statement
					.addBatch("create table tickets(mid int, mtitle varchar(50),price int, tme varchar(20), availability varchar(30), PRIMARY KEY (mid,mtitle,price,tme,availability),  FOREIGN KEY (mid, mtitle) references movies(mid, mtitle) ON DELETE CASCADE)");
			statement
					.addBatch("create table room(num int NOT NULL, capacity int, mid int, mtitle varchar(50),  PRIMARY KEY (num),  FOREIGN KEY (mid, mtitle) references movies(mid, mtitle) ON DELETE CASCADE)");
			statement
					.addBatch("create table rating(mid int, mtitle varchar(50), stars int,  PRIMARY KEY (mid, mtitle),  FOREIGN KEY (mid, mtitle) references movies(mid, mtitle) ON DELETE CASCADE)");
			statement
					.addBatch("create table showtime(mid int, mtitle varchar(50), start_time varchar(20), end_time varchar(20),  PRIMARY KEY (mid, mtitle, start_time, end_time),  FOREIGN KEY (mid, mtitle) references movies(mid, mtitle) ON DELETE CASCADE)");
			statement
					.addBatch("create table rates(usrid int, mid int, mtitle varchar(50),  PRIMARY KEY (mid, usrid, mtitle),  FOREIGN KEY (usrid) references member(usrid) ON DELETE CASCADE,  FOREIGN KEY (mid, mtitle) references rating(mid, mtitle) ON DELETE CASCADE)");
			statement
					.addBatch("create table mem_searches(usrid int, mid int, mtitle varchar(50),  PRIMARY KEY (mid, usrid, mtitle),  FOREIGN KEY (usrid) references member(usrid) ON DELETE CASCADE,  FOREIGN KEY (mid, mtitle) references movies(mid, mtitle) ON DELETE CASCADE)");
			statement
					.addBatch("create table staff_searches(usrid int, mid int, mtitle varchar(50),  PRIMARY KEY (mid, usrid, mtitle),  FOREIGN KEY (usrid) references staff(usrid) ON DELETE CASCADE,  FOREIGN KEY (mid, mtitle) references movies(mid, mtitle) ON DELETE CASCADE)");
			statement
					.addBatch("create table add_del_mod(usrid int, mid int, mtitle varchar(50),  PRIMARY KEY (mid, usrid, mtitle),  FOREIGN KEY (usrid) references staff(usrid) ON DELETE CASCADE,  FOREIGN KEY (mid, mtitle) references movies(mid, mtitle) ON DELETE CASCADE)");
			statement
					.addBatch("create table updates(usrid int, num int,  PRIMARY KEY (num, usrid),  FOREIGN KEY (num) references room(num) ON DELETE CASCADE,  FOREIGN KEY (usrid) references staff(usrid) ON DELETE CASCADE)");
			statement
					.addBatch("create table modifies(usrid int, pid int,  PRIMARY KEY (pid, usrid),  FOREIGN KEY (pid) references person(pid) ON DELETE CASCADE,  FOREIGN KEY (usrid) references staff(usrid) ON DELETE CASCADE)");
			statement
					.addBatch("create table involves(pid int, mid int, mtitle varchar(50),  PRIMARY KEY (mid, pid, mtitle),  FOREIGN KEY (pid) references person(pid) ON DELETE CASCADE,  FOREIGN KEY (mid, mtitle) references movies(mid, mtitle) ON DELETE CASCADE)");
			// statement.addBatch("create table genres(mid int, mtitle varchar(50), genre varchar(20),  PRIMARY KEY (mid, mtitle, genre),  FOREIGN KEY (mid, mtitle) references movies(mid, mtitle) ON DELETE CASCADE)");
			statement.executeBatch();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void insertMovies(Movies movies) {
		String sql = "insert into Movies values(?,?,?,?,?,?)";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, movies.getMid());
			statement.setString(2, movies.getTitle());
			statement.setString(3, movies.getPgrating());
			statement.setInt(4, movies.getReleaseyear());
			statement.setString(5, movies.getLength());
			statement.setString(6, movies.getGenre());
			statement.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public List<Movies> listMovies() {
		List<Movies> movies = new ArrayList<Movies>();
		String sql = "select * from Movies";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				int mid = result.getInt("mid");
				String mtitle = result.getString("mtitle");
				String pgrating = result.getString("pgrating");
				int releaseyear = result.getInt("release_year");
				String length = result.getString("length");
				String genre = result.getString("genre");
				Movies movie = new Movies(mid, mtitle, pgrating, releaseyear,
						length, genre);
				movies.add(movie);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			cinema.closeConnection(connection);
		}
		return movies;
	}

	public void updateMovie(int mid, String mtitle, String pgrating,
			int release_year, String length, String genre) throws SQLException {
		String sql = "update Movies set pgrating = ?, release_year = ?, length = ?, genre = ? where mid = ?";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			// statement.setString(1,mtitle);
			statement.setString(1, pgrating);
			statement.setInt(2, release_year);
			statement.setString(3, length);
			statement.setString(4, genre);
			statement.setInt(5, mid);
			statement.executeQuery();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void deleteMovie(int mid) throws SQLException {
		String sql = "delete from Movies where mid= ?";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, mid);
			statement.executeQuery();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void insertRoom(Room room) {
		String sql = "insert into Room values(?,?,?,?)";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, room.getNumber());
			statement.setInt(2, room.getCapacity());
			statement.setInt(3, room.getMid());
			statement.setString(4, room.getMtitle());
			statement.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void updateRoom(int num, int capacity, int mid, String mtitle)
			throws SQLException {
		String sql = "update Room set capacity = ?, mid = ?, mtitle = ? where num = ?";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, capacity);
			statement.setInt(2, mid);
			statement.setString(3, mtitle);
			statement.setInt(4, num);
			statement.executeQuery();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public List<Room> listRoom() {
		List<Room> rooms = new ArrayList<Room>();
		String sql = "select * from Room";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				int num = result.getInt(1);
				int capacity = result.getInt(2);
				int mid = result.getInt(3);
				String mtitle = result.getString(4);
				Room room = new Room(num, capacity, mid, mtitle);
				rooms.add(room);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			cinema.closeConnection(connection);
		}
		return rooms;
	}
	public int checkCapacity(int roomnum) throws SQLException {
		
		String sql = "select * from Room where num = ?";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, roomnum);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				int number = results.getInt(1);
				int cap = results.getInt(2);
				//cinema.closeConnection(connection);
				if (number==roomnum && cap!=0) {
					System.out.println(+number+"capc"+cap);
					return cap;		
				}
			}
			//return 0;
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			cinema.closeConnection(connection);
		}
		return 0;
	}
/*			
			String sql = "select login,pswrd from Users where login = ? and pswrd = ?";
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, login);
			statement.setString(2, password);
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				String login1 = result.getString(1);
				String pass = result.getString(2);

				cinema.closeConnection(connection);
				if (login.equals(login1) && pass.equals(password)) {
					return true;
				}
			}
			return false;
*/	
	
	public void insertShowTime(Showtime st) {
		String sql = "insert into showtime values(?,?,?,?)";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, st.getMid());
			statement.setString(2, st.getMtitle());
			statement.setString(3, st.getStart_time());
			statement.setString(4, st.getEnd_time());
			statement.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void updateShowtime(int mid, String mtitle, String start, String end)
			throws SQLException {
		String sql = "update showtime set start_time = ?, end_time = ?, mtitle = ? where mid = ?";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, start);
			statement.setString(2, end);
			statement.setString(3, mtitle);
			statement.setInt(4, mid);
			statement.executeQuery();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public List<Showtime> listshowtime() {
		List<Showtime> showt = new ArrayList<Showtime>();
		String sql = "select * from Showtime";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				int mid = result.getInt(1);
				String mtitle = result.getString(2);
				String start_time = result.getString(3);
				String end_time = result.getString(4);
				Showtime st = new Showtime(mid, mtitle, start_time, end_time);
				showt.add(st);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			cinema.closeConnection(connection);
		}
		return showt;
	}

	public void insertTicket(Tickets tick) {
		String sql = "insert into tickets values(?,?,?,?,?)";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, tick.getMid());
			statement.setString(2, tick.getM_title());
			statement.setInt(3, tick.getPrice());
			statement.setString(4, tick.getTime());
			statement.setString(5, tick.getAvailability());
			statement.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void updateTickets(String mtitle, String availability)
			throws SQLException {
		String sql = "update tickets set availability = ? where mtitle = ?";
		Connection connection = cinema.getConnection();
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setString(1, availability);
		statement.setString(2, mtitle);
		statement.executeQuery();
		cinema.closeConnection(connection);
	}

	public void insertActor(Actor actor) {
		String sql = "insert into actor values(?,?)";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, actor.getPid());
			statement.setString(2, actor.getRole());
			statement.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void updateActor(int pid, String name, String dob, String gender,
			String role) throws SQLException {
		String sql = "update person set pnm = ? , gender = ? , dob = ? where pid = ? ";
		String sql1 = "update actor set role = ? where pid = ? ";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, name);
			statement.setString(2, gender);
			statement.setString(3, dob);
			statement.setInt(4, pid);
			PreparedStatement statement1 = connection.prepareStatement(sql1);
			statement1.setString(1, role);
			statement1.setInt(2, pid);
			statement.executeQuery();
			statement1.executeQuery();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void deleteActor(int pid) throws SQLException {
		String sql = "delete from Person where pid = ?";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, pid);
			statement.executeQuery();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void insertDirector(Director director) {
		String sql = "insert into director values(?,?)";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, director.getPid());
			statement.setString(2, director.getRole());
			statement.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void updateDirector(int pid, String name, String dob, String gender,
			String role) throws SQLException {
		String sql = "update person set pnm = ? , gender = ? , dob = ? where pid = ? ";
		String sql1 = "update director set role = ? where pid = ? ";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, name);
			statement.setString(2, gender);
			statement.setString(3, dob);
			statement.setInt(4, pid);
			PreparedStatement statement1 = connection.prepareStatement(sql1);
			statement1.setString(1, role);
			statement1.setInt(2, pid);
			statement.executeQuery();
			statement1.executeQuery();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void deleteDirector(int pid) throws SQLException {
		String sql = "delete from Person where pid = ?";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, pid);
			statement.executeQuery();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void insertWriter(Writer writer) {
		String sql = "insert into writer values(?,?)";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, writer.getPid());
			statement.setString(2, writer.getRole());
			statement.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void updateWriter(int pid, String name, String dob, String gender,
			String role) throws SQLException {
		String sql = "update person set pnm = ? , gender = ? , dob = ? where pid = ? ";
		String sql1 = "update writer set role = ? where pid = ? ";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, name);
			statement.setString(2, gender);
			statement.setString(3, dob);
			statement.setInt(4, pid);
			PreparedStatement statement1 = connection.prepareStatement(sql1);
			statement1.setString(1, role);
			statement1.setInt(2, pid);
			statement.executeQuery();
			statement1.executeQuery();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void deleteWriter(int pid) throws SQLException {
		String sql = "delete from Person where pid = ?";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, pid);
			statement.executeQuery();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void insertPerson(Person person) {

		String sql = "insert into person values(?,?,?,?)";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, person.getPid());
			statement.setString(2, person.getName());
			statement.setString(4, person.getGender());
			statement.setString(3, person.getDob());
			statement.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}

	}

	public void insertUser(Users us) {

		String sql = "insert into users values ( ? , ? , ? , ? )";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, us.getusrid());
			statement.setString(2, us.getnm());
			statement.setString(3, us.getLogin());
			statement.setString(4, us.getPassword());
			statement.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}

	}

	public void insertMember(Member member) {
		String sql = "insert into member values(?)";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, member.getusrid());
			statement.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void updateMember(int usrid, String nm, String login, String password)
			throws SQLException {
		String sql = "update users set nm = ?, login = ?, pswrd = ? where usrid = ?";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, nm);
			statement.setString(2, login);
			statement.setString(3, password);
			statement.setInt(4, usrid);
			statement.executeQuery();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void deleteStaff(int usrid) throws SQLException {
		String sql = "delete from Users where usrid = ?";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, usrid);
			statement.executeQuery();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void insertStaff(Staff staff) {
		String sql = "insert into staff values(?)";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, staff.getusrid());
			statement.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void updateStaff(int usrid, String nm, String login, String password)
			throws SQLException {
		String sql = "update users set nm = ?, login = ?, pswrd = ? where usrid = ?";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, nm);
			statement.setString(2, login);
			statement.setString(3, password);
			statement.setInt(4, usrid);
			statement.executeQuery();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public void deleteMember(int usrid) throws SQLException {
		String sql = "delete from Users where usrid = ?";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, usrid);
			statement.executeQuery();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cinema.closeConnection(connection);
		}
	}

	public List<Movies> searchMovies(String movie) {
		List<Movies> movies = new ArrayList<Movies>();
		String sql = "select * from movies where mtitle = ? ";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, movie);
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				int mid = result.getInt("mid");
				String mtitle = result.getString("mtitle");
				String pgrating = result.getString("pgrating");
				int releaseyear = result.getInt("release_year");
				String length = result.getString("length");
				String genre = result.getString("genre");
				Movies movie1 = new Movies(mid, mtitle, pgrating, releaseyear,
						length, genre);
				movies.add(movie1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			cinema.closeConnection(connection);
		}
		return movies;
	}

	public List<Person> searchActor(String actor) {
		List<Person> actors = new ArrayList<Person>();
		String sql = "select * from person where pnm = ? ";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, actor);
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				int pid = result.getInt("pid");
				String name = result.getString("pnm");
				String dob = result.getString("dob");
				String gender = result.getString("gender");
				// String role1=result.getString("role");
				Person actor1 = new Person(pid, name, dob, gender);
				actors.add(actor1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			cinema.closeConnection(connection);
		}
		return actors;
	}

	public List<Person> searchDirector(String director) {
		List<Person> directors = new ArrayList<Person>();
		String sql = "select * from person where pnm = ? ";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, director);
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				int pid = result.getInt("pid");
				String name = result.getString("pnm");
				String dob = result.getString("dob");
				String gender = result.getString("gender");
				// String role1=result.getString("role");
				Person director1 = new Person(pid, name, dob, gender);
				directors.add(director1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			cinema.closeConnection(connection);
		}
		return directors;
	}

	public List<Person> searchWriter(String writer) {
		List<Person> writers = new ArrayList<Person>();
		String sql = "select * from person where pnm = ? ";
		try {
			Connection connection = cinema.getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, writer);
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				int pid = result.getInt("pid");
				String name = result.getString("pnm");
				String dob = result.getString("dob");
				String gender = result.getString("gender");
				// String role1=result.getString("role");
				Person writer1 = new Person(pid, name, dob, gender);
				writers.add(writer1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			cinema.closeConnection(connection);
		}
		return writers;
	}

	public boolean validation(String login, String password)
			throws SQLException {
		String sql = "select login,pswrd from Users where login = ? and pswrd = ?";
		Connection connection = cinema.getConnection();
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setString(1, login);
		statement.setString(2, password);
		ResultSet result = statement.executeQuery();
		while (result.next()) {
			String login1 = result.getString(1);
			String pass = result.getString(2);

			cinema.closeConnection(connection);
			if (login.equals(login1) && pass.equals(password)) {
				return true;
			}
		}
		return false;
	}

	public static void main(String[] args) throws SQLException {

		// System.out.println("Creating all the Tables in the Database...");
		// cinema.createTable();
		// System.out.println("All the Tables are ready !");
		// Movies mov=new Movies(2,"bambu","R",1990,"143 Minutes", "drama");
		// cinema.insertMovies(mov);
		// cinema.updateMovie(2, "bambu", "SS", 2001, "153", "comdu");
		
		/* List<Movies> list1=cinema.searchMovies("lol"); for(Movies
		 movi:list1) { System.out.println("Movie id : "+movi.getMid());
		 System.out.println("Movie id : "+movi.getTitle());
		 System.out.println("Movie id : "+movi.getPgrating());
		 System.out.println("Movie id : "+movi.getReleaseyear());
		 System.out.println("Movie id : "+movi.getLength());
		 System.out.println("Movie id : "+movi.getGenre()); }*/
		 
		// cinema.deleteMovie(1);
		// Room room=new Room(1,1,1,"hello");
		// cinema.insertRoom(room);
		// cinema.updateRoom(1,45,2,"bambu");
		/*
		 * List<Room> list1=cinema.listRoom(); for(Room rm:list1) {
		 * System.out.println("Movie id : "+rm.getNumber());
		 * System.out.println("Movie id : "+rm.getCapacity());
		 * System.out.println("Movie id : "+rm.getMid());
		 * System.out.println("Movie id : "+rm.getMtitle()); }
		 */
		// Showtime showtime=new Showtime(1,"hello","1:30pm","3:30pm");
		// cinema.insertShowTime(showtime);
		// cinema.updateShowtime(1, "hello", "5:00", "7:00");
		
		/*List<Person> list1=cinema.searchActor("dixu"); for(Person
		 stm:list1) { System.out.println("Movie id : "+stm.getPid());
		 System.out.println("Movie id : "+stm.getName());
		 System.out.println("Movie id : "+stm.getDob());
		 System.out.println("Movie id : "+stm.getGender());}*/
		 //System.out.println("Movie id : "+stm.getRole()); }
		 
		// Tickets tic = new Tickets(2,"bambu",50,"3:00","Yes");
		// cinema.insertTicket(tic);
		// cinema.updateTickets("bambu", "no");

		// Person person = new Person(1,"ranbir","28-9-1984","M");
		// Actor a= new Actor();
		// Actor actor = new Actor(1,"ranbir","28-9-1984","M","role1");
		// cinema.insertPerson(person);
		// cinema.insertActor(actor);

		// cinema.updateActor(1, "axf", "34545", "f", "dfdg");
		// cinema.deleteActor(1);
		// Person person = new Person(1,"ranbir","28-9-1984","M");
		// Director director = new Director(1,"ranbir","28-9-1984","M","role1");
		// cinema.insertPerson(person);
		// cinema.insertDirector(director);
		// cinema.updateDirector(1, "axf", "34545", "f", "dfdg");
		// cinema.deleteDirector(1);
		// Person person = new Person(1,"ranbir","28-9-1984","M");
		// Writer writer = new Writer(1,"ranbir","28-9-1984","M","role1");
		// cinema.insertPerson(person);
		// cinema.insertWriter(writer);
		// cinema.updateWriter(1, "axf", "34545", "f", "dfdg");
		// cinema.deleteWriter(1);
		// Users us = new Users(2,"qwerghgfhty","dixgfhfhitha","dbhaghfghrath");
		// cinema.insertUser(us);

		// Staff staff = new Staff(1,"qwerty","dixitha","dbharath");
		// cinema.insertStaff(staff);
		// cinema.updateStaff(1,"qw","tha","dath");
		// cinema.deleteStaff(1);

		// Member member = new
		// Member(2,"qwerghgfhty","dixgfhfhitha","dbhaghfghrath");
		// cinema.insertMember(member);
		// cinema.updateMember(2,"qw","tha","dath");
		// cinema.deleteMember(2);
		/*boolean val = cinema.validation("s", "s");*/
		/*int num=4;
		cinema.checkCapacity(num);*/
		//System.out.println(""+cap);
	}

}
