package cs425;

public class Member extends Users{

	private int usrid;
	
	public Member(int usrid, String nm, String login, String password) {
		super(usrid, nm, login, password);
		this.usrid = usrid;
	}

	public int getUsrid() {
		return usrid;
	}

	public void setUsrid(int usrid) {
		this.usrid = usrid;
	}

	
	
}
