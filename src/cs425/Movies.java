package cs425;

public class Movies {
	
	
	private int mid;
	private String title;
	private String pgrating;
	private int releaseyear;
	private String length;
	private String genre;

	public Movies( int mid, String title, String pgrating,
			int releaseyear, String length, String genre) {
		super();
		this.genre = genre;
		this.mid = mid;
		this.title = title;
		this.pgrating = pgrating;
		this.releaseyear = releaseyear;
		this.length = length;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPgrating() {
		return pgrating;
	}
	public void setPgrating(String pgrating) {
		this.pgrating = pgrating;
	}
	public int getReleaseyear() {
		return releaseyear;
	}
	public void setReleaseyear(int releaseyear) {
		this.releaseyear = releaseyear;
	}
	public String getLength() {
		return length;
	}
	public void setLength(String length) {
		this.length = length;
	}
		
}
