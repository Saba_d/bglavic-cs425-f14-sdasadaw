package cs425;

public class Person {
	
	private String name;
	private String gender;
	private int pid;
	private String dob;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public Person(int pid,String name, String dob, String gender ) {
		super();
		this.name = name;
		this.gender = gender;
		this.pid = pid;
		this.dob = dob;
		
	}
	
}



