package cs425;

public class Room {
	
	private int capacity;
	private int number;
	private int mid;
	private String mtitle;
	

	public Room(int capacity, int number, int mid, String mtitle) {
		super();
		this.capacity = capacity;
		this.number = number;
		this.mid = mid;
		this.mtitle = mtitle;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getMtitle() {
		return mtitle;
	}

	public void setMtitle(String mtitle) {
		this.mtitle = mtitle;
	}
	
	
	

}
