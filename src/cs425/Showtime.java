package cs425;

public class Showtime {
	
	private int mid;
	private String mtitle;
	private String start_time;
	private String end_time;
	
	
	public Showtime(int mid, String mtitle, String start_time, String end_time) {
		super();
		this.mid = mid;
		this.mtitle = mtitle;
		this.start_time = start_time;
		this.end_time = end_time;
	}

	public String getStart_time() {
		return start_time;
	}

	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getMtitle() {
		return mtitle;
	}

	public void setMtitle(String mtitle) {
		this.mtitle = mtitle;
	}
	
	
	
	

}
