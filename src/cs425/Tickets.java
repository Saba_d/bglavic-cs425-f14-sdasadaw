package cs425;

public class Tickets {

	private int price;
	private String time;
	private int mid;
	private String m_title;
	private String availability;
	
	public Tickets(int mid, String m_title,int price, String time,
			String availability) {
		super();
		this.price = price;
		this.time = time;
		this.mid = mid;
		this.m_title = m_title;
		this.availability = availability;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public String getM_title() {
		return m_title;
	}
	public void setM_title(String m_title) {
		this.m_title = m_title;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}

	
}
