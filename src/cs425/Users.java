package cs425;

public class Users {

	private String login;
	private String password;
	private int usrid;
	private String nm;
	
	public Users(int usrid,String nm,String login,String password) {
		super();
		this.login = login;
		this.password = password;
		this.usrid = usrid;
		this.nm = nm;
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getusrid() {
		return usrid;
	}
	public void setusrid(int usrid) {
		this.usrid = usrid;
	}
	public String getnm() {
		return nm;
	}
	public void setnm(String nm) {
		this.nm = nm;
	}

	
	
}
