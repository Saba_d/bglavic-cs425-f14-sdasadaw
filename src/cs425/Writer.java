package cs425;

	public class Writer extends Person {

		public Writer(int pid, String name, String dob, String gender,String role) {
			super(pid, name, dob, gender);
			this.pid = pid;
			this.role = role;
		}
		public int getPid() {
			return pid;
		}
		public void setPid(int pid) {
			this.pid = pid;
		}
		public String getRole() {
			return role;
		}
		public void setRole(String role) {
			this.role = role;
		}
		private int pid;
		private String role;
		
	}

